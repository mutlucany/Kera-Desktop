export default [
  {
    name: 'startupExtensions',
    array: ['com.kera.wallpaper'],
  },
  {
    name: 'startupApps',
    array: ['com.kera.welcome'],
  },
  {
    name: 'panelapps',
    array: [
      'com.kera.weather.panelapp',
      'com.kera.calendar.panelapp',
      'com.kera.notes.panelapp',
      'com.kera.finance.panelapp',
      'com.kera.calculator.panelapp',
      'com.kera.homeassistant.panelapp',
      'com.kera.clipboard.panelapp',
      'com.kera.settings.panelapp',
      'com.kera.translate.panelapp',
      'https://messages.google.com/web/',
      'https://web.telegram.org/',
      'com.expensive.mandala.panelapp',
      'com.kera.test.panelapp',
      'com.kera.clock.panelapp,',
    ],
  },
  {
    name: 'appmenu',
    array: [
      'apps',
      'files',
      'productivity',
      'media',
      'games',
      'internet',
      'social',
      'tools',
      'education',
      'development',
      'lifestyle',
    ],
  },
  {
    name: 'favoriteApps',
    array: [
      'com.kera.browser',
      'com.kera.settings',
      'com.kera.calculator',
      'com.kera.weather',
    ],
  },
  {
    name: 'appURLs',
    array: [
      'www.google.com',
      'google.com',
    ],
  },
];
