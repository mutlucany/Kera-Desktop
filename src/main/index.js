const {app, BrowserWindow} = require('electron');
const path = require('path');

const devTools = true;

const createWindow = () => {
  const win = new BrowserWindow({
    backgroundColor: 'black',
    fullscreen: true,
    kiosk: true,
    title: 'Kera Desktop',
    icon: 'assets/logobig.png',
    frame: false,
    webPreferences: {
      experimentalFeatures: true, // temporary for connection type detection
      devTools: devTools,
      plugins: true,
      enableBlinkFeatures: 'OverlayScrollbar',
      webviewTag: true,
      preload: path.join(__dirname, '../preload/index.js'),
    },
  });

  win.loadFile('src/renderer/index.html');
};
app.commandLine.appendSwitch('--enable-features', 'OverlayScrollbar');
app.whenReady().then(() => {
  createWindow();

  app.on('activate', () => {
    if (BrowserWindow.getAllWindows().length === 0) {
      createWindow();
    }
  });
});

app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit();
  }
});
